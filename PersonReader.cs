using System.IO;
using _5e.Formatters;

namespace _5e
{
    public class PersonReader : IPersonReader
    {
        private readonly StreamReader _reader;
        private readonly IPersonFormatter _formatter;

        public PersonReader(StreamReader reader, IPersonFormatter formatter)
        {
            _reader = reader;
            _formatter = formatter;
        }

        public Person? TryReadNextPerson()
        {
            string line = _reader.ReadLine();
            if (line is not null)
            {
                try
                {
                    return _formatter.Format(line);
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }
    }
}
