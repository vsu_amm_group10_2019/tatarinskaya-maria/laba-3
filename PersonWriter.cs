using System.IO;
using _5e.Formatters;

namespace _5e
{
    public class PersonWriter : IPersonWriter
    {
        private readonly StreamWriter _writer;
        private readonly IPersonFormatter _formatter;

        public PersonWriter(StreamWriter writer, IPersonFormatter formatter)
        {
            _writer = writer;
            _formatter = formatter;
        }

        public void WritePerson(Person person)
            => _writer.WriteLine(_formatter.Format(person));
    }
}
