using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _5e.Collections
{
    public class HashTable<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        private const double FullnessMax = 0.80;
        private KeyValuePair<TKey, TValue>?[] _arr;
        private readonly IEqualityComparer<TKey> _keyComparer;
        private readonly IAddressingStepStrategy<TKey, TValue> _stepStrategy;

        public int Count { get; private set; }

        public int Capacity
            => _arr.Length;

        public bool IsReadOnly
            => false;

        public double Fullness
            => (double)Count / Capacity;

        public ICollection<TKey> Keys
            => this
            .Select(pair => pair.Key)
            .ToList();

        public ICollection<TValue> Values
            => this
            .Select(pair => pair.Value)
            .ToList();

        public TValue this[TKey key] 
        { 
            get 
            {
                if (key is null)
                    throw new ArgumentNullException(nameof(key));

                int index = GetRealPlace(key);
                if (_arr[index].HasValue)
                    return _arr[index].Value.Value;
                else
                    throw new KeyNotFoundException("Ключ не содержится в коллекции.");
            }
            set => Add(key, value);
        }

        public HashTable()
            : this(stepStrategy: null, comparer: null)
        {
            
        }

        public HashTable(IAddressingStepStrategy<TKey, TValue> stepStrategy, IEqualityComparer<TKey> comparer = null)
            : this (1, stepStrategy, comparer)
        {
            
        }

        public HashTable(int capacity, IAddressingStepStrategy<TKey, TValue> stepStrategy, IEqualityComparer<TKey> comparer = null)
        {
            if (stepStrategy is null)
                throw new ArgumentNullException(nameof(stepStrategy));

            if (capacity < 1)
                throw new ArgumentOutOfRangeException(nameof(capacity), "Значение должно быть больше 0.");
            
            _keyComparer = comparer ?? EqualityComparer<TKey>.Default;
            _stepStrategy = stepStrategy;
            _arr = new KeyValuePair<TKey, TValue>?[capacity];
        }

        private void Resize()
        {
            var newHashTable = new HashTable<TKey, TValue>(Capacity * 2, _stepStrategy, _keyComparer);
            foreach (KeyValuePair<TKey, TValue> pair in this)
                newHashTable.Add(pair.Key, pair.Value);
            
            _arr = newHashTable._arr;
            Count = newHashTable.Count;
        }

        private int GetSupposedPlace(TKey key)
            => _keyComparer.GetHashCode(key) % Capacity;

        private int GetRealPlace(TKey key)
        {
            if (Fullness >= FullnessMax)
                Resize();
            
            int index = GetSupposedPlace(key);
            while (true)
            {
                if (!_arr[index].HasValue || _keyComparer.Equals(_arr[index].Value.Key, key))
                    return index;
                index = _stepStrategy.GetNext(this, key, index);
            }
        }

        private int GetDistance(int firstIndex, int secondIndex)
            => (secondIndex + Capacity - firstIndex) % Capacity;

        private static void Swap<T>(ref T first, ref T second)
        {
            T temp = first;
            first = second;
            second = temp;
        }

        public void Add(TKey key, TValue value)
        {
            if (key is null)
                throw new ArgumentNullException(nameof(key));

            int index = GetRealPlace(key);
            if (_arr[index].HasValue)
                throw new ArgumentException("Элемент с таким ключом уже существует.");

            _arr[index] = new KeyValuePair<TKey, TValue>(key, value);
            ++Count;
        }

        public void Clear()
        {
            Count = 0;
            _arr = new KeyValuePair<TKey, TValue>?[_arr.Length];
        }
        
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex = 0)
        {
            if (array is null)
                throw new ArgumentNullException(nameof(array));

            if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException(nameof(arrayIndex), "Значение должно быть больше 0.");
            
            int pairsLength = (this as IEnumerable<KeyValuePair<TKey, TValue>>).Count();
            if (arrayIndex + pairsLength > array.Length)
                throw new ArgumentException("Число элементов в исходной коллекции больше доступного места от положения, заданного значением параметра index, до конца массива назначения array.");

            foreach (KeyValuePair<TKey, TValue> pair in this)
                array[arrayIndex++] = pair;
        }

        public bool Remove(TKey key)
        {
            if (key is null)
                return false;
            
            int window = GetRealPlace(key);
            if (!_arr[window].HasValue)
                return false;

            _arr[window] = null;
            int next = _stepStrategy.GetNext(this, key, window);
            while (_arr[next].HasValue)
            {
                if (GetDistance(GetSupposedPlace(_arr[next].Value.Key), next) >= GetDistance(window, next))
                {
                    Swap(ref _arr[next], ref _arr[window]);
                    window = next;
                }
                next = _stepStrategy.GetNext(this, key, next);
            }
            
            --Count;
            return true;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
            => _arr
                .Where(pair => pair is not null)
                .Select(pair => pair.Value)
                .GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();

        public bool ContainsKey(TKey key)
            => _arr[GetRealPlace(key)].HasValue;

        public bool TryGetValue(TKey key, out TValue value)
        {
            value = default;

            if (key is null)
                return false;

            int index = GetRealPlace(key);
            if (_arr[index].HasValue)
            {
                value = _arr[index].Value.Value;
                return true;
            }
            
            return false;
        }

        public override string ToString()
        {
            const string DictionaryPattern = "{{\"{0}\", \"{1}\"}}";
            var builder = new StringBuilder("[");
            var pairs = ((IEnumerable<KeyValuePair<TKey, TValue>>)this).ToArray();
            if (pairs.Length > 0)
            {
                builder.AppendFormat(DictionaryPattern, pairs[0].Key, pairs[0].Value);
                for (int i = 1; i < pairs.Length; ++i)
                    builder.Append(", ").AppendFormat(DictionaryPattern, pairs[i].Key, pairs[i].Value);
            }
            builder.Append(']');
            return builder.ToString();
        }
    }
}