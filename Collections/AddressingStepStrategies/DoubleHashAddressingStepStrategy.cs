using System.Collections.Generic;
using _5e.Collections;

namespace _5e
{
    public class DoubleHashAddressingStepStrategy<TKey, TValue> : IAddressingStepStrategy<TKey, TValue>
    {
        private readonly IEqualityComparer<TKey> _keyComparer;

        public DoubleHashAddressingStepStrategy(IEqualityComparer<TKey> keyComparer = null)
            => _keyComparer = keyComparer ?? EqualityComparer<TKey>.Default;

        public int GetNext(HashTable<TKey, TValue> table, TKey key, int index)
        {
            var result = (index + _keyComparer.GetHashCode(key) + 1) % table.Capacity;
            return index == result ? (result + 1) % table.Capacity : result;
        }
    }
}
