namespace _5e.Collections
{
    public interface IAddressingStepStrategy<TKey, TValue>
    {
        int GetNext(HashTable<TKey, TValue> table, TKey key, int index);
    }
}