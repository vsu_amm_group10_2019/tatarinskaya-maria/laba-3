using System;

namespace _5e.Collections
{
    public class LinearAddressingStepStrategy<TKey, TValue> : AddressingStepStrategy<TKey, TValue>
    {
        private readonly int _step;

        public LinearAddressingStepStrategy(int step = 1)
        {
            if (step < 1)
                throw new ArgumentOutOfRangeException(nameof(step), "Значение должно быть больше 0.");
            
            _step = step;
        }

        public override int GetNext(HashTable<TKey, TValue> table, TKey key, int index)
            => (index + _step) % table.Capacity;
    }
}