﻿using System;
using _5e.Formatters;

namespace _5e
{
    public class MessageBoxFormatter : IPersonFormatter
    {
        public string Format(Person person)
            => $"ФИО: {person.Fullname}\nТелефон: {person.PhoneNumber}\nАдрес: {person.Address}";

        public Person Format(string person)
            => throw new NotImplementedException();
    }
}
