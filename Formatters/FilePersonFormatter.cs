using _5e.Formatters;

namespace _5e
{
    public class FilePersonFormatter : IPersonFormatter
    {
        public string Format(Person person)
            => $"{person.Fullname};{person.PhoneNumber};{person.Address}";

        public Person Format(string person)
        {
            string[] temp = person.Split(';');
            return new Person(temp[0], new PhoneNumber(temp[1]), temp[2]);
        }
    }
}
