﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using _5e.Collections;
using _5e.Formatters;

namespace _5e
{
    public partial class MainForm : Form
    {
        private readonly IPersonFormatter _personFormatter;
        private readonly IPersonFormatter _messageBoxFormatter;
        private readonly IAddressingStepStrategy<PhoneNumber, Person> _hashStrategy;
        private readonly HashTable<PhoneNumber, Person> _table;
        private PhoneNumber _tempBuffer;

        public MainForm()
        {
            InitializeComponent();
            openTableDialog.InitialDirectory = Environment.CurrentDirectory;
            saveTableDialog.InitialDirectory = Environment.CurrentDirectory;
            _tempBuffer = null;
            _personFormatter = new FilePersonFormatter();
            _messageBoxFormatter = new MessageBoxFormatter();
            _hashStrategy = new DoubleHashAddressingStepStrategy<PhoneNumber, Person>();
            _table = new HashTable<PhoneNumber, Person>(_hashStrategy);
        }

        private void CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            var table = (DataGridView)sender;
            if (table.Columns[e.ColumnIndex].Name == "PhoneNumber")
            {
                var z = (string)table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                _tempBuffer = new PhoneNumber(z);
            }
        }

        private void FindClicked(object sender, EventArgs e)
        {
            if (_table.ContainsKey(new PhoneNumber(findTextBox.Text)))
            {
                Person person = _table[new PhoneNumber(findTextBox.Text.Trim())];
                MessageBox.Show(this, _messageBoxFormatter.Format(person), "Человек найден", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show(this, "Не удалось найти человека с заданным номером телефона.", "Человек не найден", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void LoadClick(object sender, EventArgs e)
        {
            if (openTableDialog.ShowDialog() == DialogResult.OK)
            {
                _table.Clear();

                using var streamReader = new StreamReader(openTableDialog.FileName);
                var reader = new PersonReader(streamReader, _personFormatter);
                while (true)
                {
                    Person? person = reader.TryReadNextPerson();
                    if (person.HasValue)
                        _table.Add(person.Value.PhoneNumber, person.Value);
                    else
                        break;
                }

                tableView.Rows.Clear();
                foreach (Person person in _table.Values)
                    tableView.Rows.Add(person.Fullname, person.PhoneNumber.ToString(), person.Address);
            }
        }

        private void SaveClick(object sender, EventArgs e)
        {
            if (saveTableDialog.ShowDialog() == DialogResult.OK)
            {
                using var streamWriter = new StreamWriter(saveTableDialog.FileName);
                var writer = new PersonWriter(streamWriter, _personFormatter);
                foreach (var value in _table.Values)
                    writer.WritePerson(value);
            }
        }

        private void CellEdit(object sender, DataGridViewCellEventArgs e)
        {
            const string AvailableSymbols = "0123456789()-+ ";
            var table = (DataGridView)sender;
            if (table.Columns[e.ColumnIndex].Name == "PhoneNumber")
            {
                string phoneNumber = (string)table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                var isCorrect = phoneNumber?.Distinct().Except(AvailableSymbols).Count() < 1;
                if (!isCorrect)
                    table.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = _tempBuffer.ToString();
                else
                {
                    _table.Remove(_tempBuffer);
                    var fullname = (string)table.Rows[e.RowIndex].Cells[0].Value;
                    var number = new PhoneNumber((string)table.Rows[e.RowIndex].Cells[1].Value);
                    var address = (string)table.Rows[e.RowIndex].Cells[2].Value;
                    var person = new Person(fullname, number, address);
                    try
                    {
                        _table.Add(number, person);
                    } catch (ArgumentException)
                    {
                        
                    }
                }
            }
        }

        private void EnterPressed(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                findButton.PerformClick();
        }
    }
}