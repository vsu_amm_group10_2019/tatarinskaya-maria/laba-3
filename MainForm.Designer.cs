﻿
namespace _5e
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableGroupBox = new System.Windows.Forms.GroupBox();
            this.tableView = new System.Windows.Forms.DataGridView();
            this.FIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loadDataButton = new System.Windows.Forms.Button();
            this.saveDataButton = new System.Windows.Forms.Button();
            this.findButton = new System.Windows.Forms.Button();
            this.findTextBox = new System.Windows.Forms.MaskedTextBox();
            this.openTableDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveTableDialog = new System.Windows.Forms.SaveFileDialog();
            this.tableGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableView)).BeginInit();
            this.SuspendLayout();
            // 
            // tableGroupBox
            // 
            this.tableGroupBox.Controls.Add(this.tableView);
            this.tableGroupBox.Location = new System.Drawing.Point(4, 2);
            this.tableGroupBox.Name = "tableGroupBox";
            this.tableGroupBox.Size = new System.Drawing.Size(391, 160);
            this.tableGroupBox.TabIndex = 0;
            this.tableGroupBox.TabStop = false;
            this.tableGroupBox.Text = "Таблица:";
            // 
            // tableView
            // 
            this.tableView.AllowUserToResizeColumns = false;
            this.tableView.AllowUserToResizeRows = false;
            this.tableView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.tableView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.tableView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FIO,
            this.PhoneNumber,
            this.Address});
            this.tableView.Location = new System.Drawing.Point(8, 22);
            this.tableView.Name = "tableView";
            this.tableView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.tableView.RowTemplate.Height = 25;
            this.tableView.Size = new System.Drawing.Size(376, 132);
            this.tableView.TabIndex = 4;
            this.tableView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.CellBeginEdit);
            this.tableView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellEdit);
            // 
            // FIO
            // 
            this.FIO.HeaderText = "ФИО";
            this.FIO.Name = "FIO";
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.HeaderText = "Номер телефона";
            this.PhoneNumber.Name = "PhoneNumber";
            // 
            // Address
            // 
            this.Address.HeaderText = "Адрес";
            this.Address.Name = "Address";
            this.Address.Width = 135;
            // 
            // loadDataButton
            // 
            this.loadDataButton.Location = new System.Drawing.Point(7, 168);
            this.loadDataButton.Name = "loadDataButton";
            this.loadDataButton.Size = new System.Drawing.Size(244, 23);
            this.loadDataButton.TabIndex = 0;
            this.loadDataButton.Text = "Загрузить данные";
            this.loadDataButton.UseVisualStyleBackColor = true;
            this.loadDataButton.Click += new System.EventHandler(this.LoadClick);
            // 
            // saveDataButton
            // 
            this.saveDataButton.Location = new System.Drawing.Point(7, 197);
            this.saveDataButton.Name = "saveDataButton";
            this.saveDataButton.Size = new System.Drawing.Size(124, 23);
            this.saveDataButton.TabIndex = 1;
            this.saveDataButton.Text = "Сохранить данные";
            this.saveDataButton.UseVisualStyleBackColor = true;
            this.saveDataButton.Click += new System.EventHandler(this.SaveClick);
            // 
            // findButton
            // 
            this.findButton.Location = new System.Drawing.Point(257, 169);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(134, 51);
            this.findButton.TabIndex = 3;
            this.findButton.Text = "Найти";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler(this.FindClicked);
            // 
            // findTextBox
            // 
            this.findTextBox.Location = new System.Drawing.Point(137, 197);
            this.findTextBox.Mask = "+7 (999) 000-00-00";
            this.findTextBox.Name = "findTextBox";
            this.findTextBox.Size = new System.Drawing.Size(114, 23);
            this.findTextBox.TabIndex = 2;
            this.findTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.EnterPressed);
            // 
            // openTableDialog
            // 
            this.openTableDialog.Filter = "Текстовый файл (*.txt)|*.txt|Все файлы|*.*";
            this.openTableDialog.Title = "Открыть файл";
            // 
            // saveTableDialog
            // 
            this.saveTableDialog.Filter = "Текстовый файл (*.txt)|*.txt|Все файлы|*.*";
            this.saveTableDialog.Title = "Сохранить файл";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 226);
            this.Controls.Add(this.findTextBox);
            this.Controls.Add(this.saveDataButton);
            this.Controls.Add(this.findButton);
            this.Controls.Add(this.loadDataButton);
            this.Controls.Add(this.tableGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Хеширование - 5e";
            this.tableGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox tableGroupBox;
        private System.Windows.Forms.Button loadDataButton;
        private System.Windows.Forms.Button saveDataButton;
        private System.Windows.Forms.DataGridView tableView;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.MaskedTextBox findTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn FIO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.OpenFileDialog openTableDialog;
        private System.Windows.Forms.SaveFileDialog saveTableDialog;
    }
}