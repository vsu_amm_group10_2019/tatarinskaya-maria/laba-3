namespace _5e
{
    public interface IPersonWriter
    {
        void WritePerson(Person person);
    }
}
