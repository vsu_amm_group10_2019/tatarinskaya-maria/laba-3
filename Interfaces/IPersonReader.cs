namespace _5e
{
    public interface IPersonReader
    {
        Person? TryReadNextPerson();
    }
}
