namespace _5e.Formatters
{
    public interface IPersonFormatter
    {
        string Format(Person person);
        Person Format(string person);
    }
}
