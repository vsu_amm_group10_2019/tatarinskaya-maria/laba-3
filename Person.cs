using System;

namespace _5e
{
    public class PhoneNumber : IEquatable<PhoneNumber>
    {
        private readonly string  _number;

        public PhoneNumber(string number)
            => _number = number;

        public bool Equals(PhoneNumber other)
            => _number == other._number;

        public override int GetHashCode()
        {
            int sum = 0;
            foreach (char symbol in _number)
                if (char.IsDigit(symbol))
                    sum += symbol;
            return sum;
        }

        public override string ToString()
            => _number.ToString();
    }

    public struct Person
    {
        public PhoneNumber PhoneNumber { get; }
        public string Fullname { get; }
        public string Address { get; }

        public Person(string fullname, PhoneNumber phoneNumber, string address)
        {
            PhoneNumber = phoneNumber;
            Fullname = fullname;
            Address = address;
        }

        
    }
}
