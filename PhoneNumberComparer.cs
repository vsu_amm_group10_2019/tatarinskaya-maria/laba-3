using System.Collections.Generic;

namespace _5e
{
    public class PhoneNumberComparer : IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
            => x == y;

        public int GetHashCode(string obj)
        {
            int sum = 0;
            foreach (char symbol in obj)
                if (char.IsDigit(symbol))
                    sum += symbol;
            return sum;
        }
    }
}
